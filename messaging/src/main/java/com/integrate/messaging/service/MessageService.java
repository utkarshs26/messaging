package com.integrate.messaging.service;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import com.integrate.messaging.conf.JmsConfig;
import com.integrate.messaging.domain.order.Order;

/**
 * A service that sends and receives JMS messages. 
 * 
 */
public class MessageService 
{
/*	@Autowired
	private JmsTemplate jmsTemplate;*/
	
//	@Value("OrderQueue")
	private final String destination = "OrderQueue";

	/**
	 * Sends a message to a queue.
	 * 
	 * @param text Message text.
	 */
	public void sendMessage(final Order order) {
/*		jmsTemplate.send(destination, new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				return session.createObjectMessage(order);
			}
		});*/
		
		JmsConfig.getJmsTemplate().send(destination, new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				return session.createObjectMessage(order);
			}
		});
		
	}
	
	/**
	 * Receives a message from a queue.
	 * 
	 * @return Order.
	 * @throws JMSException
	 */
	public void readMessage() throws JMSException {
		Order order = null;

//		Message msg = jmsTemplate.receive(destination);
		Message msg = JmsConfig.getJmsTemplate().receive(destination);
		
		if(msg instanceof Order) {
			order = (Order) msg;
		}
		
		if (order != null) {
	    	System.out.println("OrderNumber = " + order.getOrderNumber());
	    	System.out.println("Total = " + order.getTotal());
	    	System.out.println("FirstName = " + order.getCustomer().getFirstName());
	    	System.out.println("LastName = " + order.getCustomer().getLastName());
		}
		
		
//		return order;
	}
}
