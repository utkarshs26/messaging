package com.integrate.messaging;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

import com.integrate.messaging.service.MessageService;

@SpringBootApplication
@EnableJms
public class MessagingApplication {

	public static void main(String[] args) {
		SpringApplication.run(MessagingApplication.class, args);
		
		try {
			new MessageService().readMessage();
		} catch (Exception ex) {
			ex.printStackTrace();			
		}
		
	}

}

